package Resources;

import COAPKerbDomain.AuthenticationServer.ASResponse;
import COAPKerbDomain.AuthenticationServer.ClientCredentials;
import COAPKerbDomain.KerbStates;
import COAPKerbDomain.TicketGrantingTicket;
import COAPKerbDomain.AuthenticationServer.ClientTGTRequest;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.CREATED;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.UNAUTHORIZED;

public class AuthenticateService extends CoapResource {

    private String tgtKey;
    private String tgsKey;

    private String userPassword = "iotlab1@";
    private static String userName = "user1";
    private static String dummyIP = "192.168.1.1";

    public AuthenticateService(String tgtKey, String tgsKey) {
        super("Authenticate");
        this.tgtKey = tgtKey;
        this.tgsKey = tgsKey;
        getAttributes().setTitle("Authenticate Resource");
    }

    @Override
    public void handleGET(CoapExchange exchange) {
        System.out.println("A get request came from: " + exchange.getSourceAddress().getHostAddress());
        System.out.println("Content format is: " + exchange.getRequestOptions().getContentFormat());
        //TODO implement the response of statistics about the service
        exchange.respond("Some dummy data");
    }

    @Override
    public void handlePOST(CoapExchange exchange) {
        exchange.accept();
        int format = exchange.getRequestOptions().getContentFormat();

        switch (format) {
            case MediaTypeRegistry.TEXT_XML: {
                String xml = exchange.getRequestText();
                String responseTxt = "Received XML: '" + xml + "'";
                System.out.println(responseTxt);
                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.TEXT_PLAIN: {
                // ...
                String plain = exchange.getRequestText();
                String responseTxt = "Received text: '" + plain + "'";
                System.out.println(responseTxt);
                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.APPLICATION_CBOR:
                processCBORRequest(exchange);
                break;
            default:
                byte[] bytes = exchange.getRequestPayload();
                System.out.println("Received bytes: " + bytes);
                exchange.respond(CREATED);
                break;
        }
    }

    private void processCBORRequest(CoapExchange exchange) {
        ClientTGTRequest userTicket = ClientTGTRequest.toObject(exchange.getRequestPayload());
        ASResponse authResponse = new ASResponse();
        ClientCredentials clientCred = new  ClientCredentials();
        try {
            if(validateUser(exchange, userTicket)){
                clientCred.setTGT(new TicketGrantingTicket("TGS"),tgtKey);
                clientCred.setTGSSessionKey(tgsKey);
                clientCred.statusCode = KerbStates.ACCESS_GRANTED;
                authResponse.setContent(clientCred,userPassword);
                exchange.respond(CREATED,authResponse.toCBOR());
            } else {
                exchange.respond(UNAUTHORIZED,authResponse.toCBOR());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validateUser(CoapExchange exchange, ClientTGTRequest clientTGTRequest) throws Exception {

        System.out.println("User IP is: "+exchange.getSourceAddress().getHostAddress());
        System.out.println("User password is: "+userPassword);

        String userFixedIP = clientTGTRequest.getUserSignature(userPassword);

        if(userFixedIP.equals(dummyIP)){
            return true;
        } else
            return false;
    }
}
