package Resources;

import COAPKerbDomain.AES;
import COAPKerbDomain.Resource;
import COAPKerbDomain.TicketGrantingServer.TGSRequest;
import COAPKerbDomain.TicketGrantingServer.TGSRequestContent;
import COAPKerbDomain.TicketGrantingServer.TGSResponse;
import COAPKerbDomain.TicketGrantingServer.TGSResponseContent;
import COAPKerbDomain.TicketGrantingTicket;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.HashMap;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.CREATED;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.NOT_FOUND;
import static org.eclipse.californium.core.coap.CoAP.ResponseCode.UNAUTHORIZED;

public class TicketGrantingService extends CoapResource {

    private String tgtKey;
    private String tgsKey;
    private HashMap<String, Resource> resourceDB;


    public TicketGrantingService(HashMap resourceDB, String tgtKey, String tgsKey) {
        super("TicketGrantingService");
        this.resourceDB = resourceDB;
        this.tgtKey = tgtKey;
        this.tgsKey = tgsKey;
        getAttributes().setTitle("Grant tickets");
    }

    @Override
    public void handlePOST(CoapExchange exchange){
        exchange.accept();
        int format = exchange.getRequestOptions().getContentFormat();
        switch (format) {
            case MediaTypeRegistry.TEXT_XML: {
//                String xml = exchange.getRequestText();
//                String responseTxt = "Received XML: '" + xml + "'";
//                System.out.println(responseTxt);
//                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.TEXT_PLAIN: {
                // ...
//                String plain = exchange.getRequestText();
//                String responseTxt = "Received text: '" + plain + "'";
//                System.out.println(responseTxt);
//                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.APPLICATION_CBOR:
                processCBORRequest(exchange);
                break;
            default:
                byte[] bytes = exchange.getRequestPayload();
                System.out.println("Received bytes: " + bytes);
                exchange.respond(CREATED);
                break;
        }
    }

    private void processCBORRequest(CoapExchange exchange) {
        TGSRequest tgsReq = null;
        TGSResponse tgsResponse = new TGSResponse();
        try {
            tgsReq = TGSRequest.toObject(exchange.getRequestPayload());
            TGSRequestContent tgsReqContent = tgsReq.getContent(tgsKey);

            if(validateTGT(exchange, tgsReqContent)){
                String resourceName = tgsReqContent.resource;
                TGSResponseContent tgsResponseContent = new TGSResponseContent();
                if(resourceDB.containsKey(resourceName))
                {
                    Resource res = resourceDB.get(resourceName);
                    tgsResponseContent.createResourceTicket(res.resourceName,res.rsourceKey);
                    res.rsourceKey = null;
                    tgsResponseContent.resourceInfo = res;
                    tgsResponseContent.sessionKey = AES.generateSecretKey();
                    tgsResponse.setContent(tgsResponseContent,tgsKey);

                    exchange.respond(CREATED, tgsResponse.toCBOR());
                }else{
                    exchange.respond(NOT_FOUND, tgsResponse.toCBOR());
                }
            }else{
                exchange.respond(UNAUTHORIZED,tgsResponse.toCBOR());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean validateTGT(CoapExchange exchange, TGSRequestContent tgsRequestContent) throws Exception {
        System.out.println(exchange.getSourceAddress().getHostAddress());
        System.out.println("Client is requesting " + tgsRequestContent.resource);
        TicketGrantingTicket tgt = tgsRequestContent.getTGT(tgtKey);
        if(tgt.resourceName.equals("TGS")){
            return true;
        }
        return false;
    }
}
