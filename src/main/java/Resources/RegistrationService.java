package Resources;

import COAPKerbDomain.Resource;
import COAPKerbDomain.TicketGrantingServer.TGSRequestContent;
import COAPKerbDomain.TicketGrantingServer.TGSResponse;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import java.util.HashMap;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.CREATED;

public class RegistrationService extends CoapResource {

    private  HashMap<String, Resource> resourceDB;

    public RegistrationService(HashMap resourceDB) {
        super("RegistrationService");
        this.resourceDB = resourceDB;
        getAttributes().setTitle("Register new resources");
    }

    @Override
    public void handlePOST(CoapExchange exchange) {
        exchange.accept();
        int format = exchange.getRequestOptions().getContentFormat();
        switch (format) {
            case MediaTypeRegistry.TEXT_XML: {
//                String xml = exchange.getRequestText();
//                String responseTxt = "Received XML: '" + xml + "'";
//                System.out.println(responseTxt);
//                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.TEXT_PLAIN: {
                // ...
//                String plain = exchange.getRequestText();
//                String responseTxt = "Received text: '" + plain + "'";
//                System.out.println(responseTxt);
//                exchange.respond(CREATED, responseTxt);
                break;
            }
            case MediaTypeRegistry.APPLICATION_CBOR:
                processCBORRequest(exchange);
                break;
            default:
                byte[] bytes = exchange.getRequestPayload();
                System.out.println("Received bytes: " + bytes);
                exchange.respond(CREATED);
                break;
        }
    }

    private void processCBORRequest(CoapExchange exchange){




    }

}
