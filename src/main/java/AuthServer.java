import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.HashMap;

import COAPKerbDomain.AES;
import COAPKerbDomain.Resource;
import Resources.AuthenticateService;

import Resources.TicketGrantingService;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;

public class AuthServer extends CoapServer {

    private static final int COAP_PORT = NetworkConfig.getStandard().getInt(NetworkConfig.Keys.COAP_PORT);

    private static String tgtKey;
    private static String tgsKey;

    private static HashMap<String, Resource> resourceDB = new HashMap();

    public static void main(String[] args) {
        try {
            //Generate keys
            tgtKey = AES.generateSecretKey();
            tgsKey = AES.generateSecretKey();

            System.out.println(AES.generateSecretKey());
            setFakeResData();
            // create server
            AuthServer server = new AuthServer();
            // add endpoints on all IP addresses
            server.addEndpoints();
            server.start();
            System.out.println("Service has started");

        } catch (Exception e) {
            System.err.println("Failed to initialize server: " + e.getMessage());
        }
    }

    private void addEndpoints() {
        System.out.println("--------Binding COAP to the following addresses--------");
        for (InetAddress addr : EndpointManager.getEndpointManager().getNetworkInterfaces()) {
            // binds to IPv4 addresses and localhost
            if (addr instanceof Inet4Address || addr.isLoopbackAddress()) {
                InetSocketAddress bindToAddress = new InetSocketAddress(addr, COAP_PORT);
                addEndpoint(new CoapEndpoint(bindToAddress));
                System.out.println(addr.getHostAddress());
            }
        }
    }

    public AuthServer() throws SocketException {
        add(new AuthenticateService(tgtKey,tgsKey));
        add(new TicketGrantingService(resourceDB,tgtKey,tgsKey));
    }

    private static void setFakeResData(){

        Resource res = new Resource();
        res.rsourceKey = "[5, -54, 64, -79, 37, 2, -120, 36, 63, -66, 85, 72, -61, -71, -71, 7]";
        res.resourceName = "camera";
        res.resourceCOAPURI       = "coap://192.168.1.137:5683/Camera";
        res.resourceKerberizedURI = "coap://192.168.1.137:5683/KCamera";

        resourceDB.put("camera",res);
    }

}
